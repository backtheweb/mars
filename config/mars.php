<?php

return [
    'admin' => [
        'name'     => env('MARS_ADMIN_NAME',     'admin'),
        'email'    => env('MARS_ADMIN_EMAIL'),
        'password' => env('MARS_ADMIN_PASSWORD'),
    ]
];
