<?php

namespace Backtheweb\Mars\Actions;

use Illuminate\Support\Facades\Storage;
use Spatie\DbDumper\Databases\MySql;

class DumpDataBaseAction
{
    /**
     * @throws \Exception
     */
    public function execute() : string {

        $fileName = 'backups/' . config('database.connections.mysql.database') . '.' . date('Y-m-d_h-m-i') . '.sql';
        $file     = Storage::disk('local')->path($fileName);

        if(!is_dir(dirname($file))){
            mkdir(dirname($file), 0777, true);
        }

        MySql::create()
            ->setHost(      config('database.connections.mysql.host'))
            ->setDbName(    config('database.connections.mysql.database'))
            ->setUserName(  config('database.connections.mysql.username'))
            ->setPassword(  config('database.connections.mysql.password'))
            ->dumpToFile($file);

        exec('gzip ' . $file);

        return $file . '.gz';
    }
}
