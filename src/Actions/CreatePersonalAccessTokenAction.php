<?php

namespace Backtheweb\Mars\Actions;

use App\Models\User;

class CreatePersonalAccessTokenAction
{
    public function execute(User $user, string $name = 'default', array $abilities = ['*']): string
    {

        $user->tokens()->where('name', $name)->delete();

        $token = $user->createToken($name, $abilities);

        return $token->plainTextToken;
    }
}
