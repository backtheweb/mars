<?php

namespace Backtheweb\Mars\Actions;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadAction
{
    /**
     * @return int
     */
    public static function maxUploadSize(): int
    {
        $max_upload = min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
        $max_upload = str_replace('M', '', $max_upload);
        return $max_upload * 1024;
    }

    public function execute(UploadedFile $file, string $path = null, string $disk = 'local'): string
    {
        $fileName = sprintf('%s-%s.%s',
            uniqid(),
            Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME), config('app.locale', 'en_GB')),
            strtolower($file->getClientOriginalExtension())
        );

        $file->storeAs($path, $fileName, ['disk' => $disk]);

        return Storage::disk($disk)->get( join('/' , [$path,$fileName] ));
    }
}
