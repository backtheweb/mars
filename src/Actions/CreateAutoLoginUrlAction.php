<?php

namespace Backtheweb\Mars\Actions;

use App\Models\User;

class CreateAutoLoginUrlAction
{

    public function __construct( public string $tokenName, public array $abilities = ['*'] )
    {

    }

    public function execute(User $user, string $redirect = null): string
    {
        $token = $user->tokens()->where($this->tokenName)->first();

        if (!$token) {
            $new = $user->createToken($this->tokenName, $this->abilities);
            $token = $new->accessToken;
        }

        return route('login.token', [
            'token'    => encrypt($token->token),
            'redirect' => $redirect,
        ]);
    }
}
