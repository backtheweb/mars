<?php

namespace Backtheweb\Mars\Console\Database;

use Backtheweb\Mars\Traits\Commands\Helpers;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use Spatie\DbDumper\Databases\MySql;

class DumpCommand extends Command
{

    use Helpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mars:db:dump {--disk=local} {{--zip}}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dump database {--disk=local}';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        /** @var FilesystemAdapter $disk */
        $disk     = Storage::disk($this->option('disk'));
        $fileName = 'db/' . config('database.connections.mysql.database') . '-' . date('Y-m-d_H-i-s') . '.sql';
        $file     = $disk->path($fileName);

        if(!is_dir(dirname($file))){
            mkdir(dirname($file), 0777, true);
        }

        MySql::create()
            ->setHost(      config('database.connections.mysql.host'))
            ->setDbName(    config('database.connections.mysql.database'))
            ->setUserName(  config('database.connections.mysql.username'))
            ->setPassword(  config('database.connections.mysql.password'))
            ->dumpToFile($file);

        if($this->option('zip')){
            exec('gzip ' . $file);
        }

        $this->lineInfo('Dump to ' . $fileName . ' from ' . config('database.connections.mysql.database') . ' database');

        return Command::SUCCESS;
    }
}
