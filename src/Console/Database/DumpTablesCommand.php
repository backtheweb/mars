<?php

namespace Backtheweb\Mars\Console\Database;

use Backtheweb\Mars\Traits\Commands\Helpers;
use Doctrine\DBAL\Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Exceptions\CannotSetParameter;
use Symfony\Component\Console\Command\Command as CommandAlias;

class DumpTablesCommand extends Command
{

    use Helpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mars:db:dump-tables {--disk=local}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump database';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws CannotSetParameter|Exception
     */
    public function handle(): int
    {
        /** @var FilesystemAdapter $disk */
        $disk   = Storage::disk($this->option('disk'));
        $tables = collect(DB::connection()->getDoctrineSchemaManager()->listTableNames());

        $tables->each(function($table, $index) {

            if($index < 10){
                $i = ' ' . $index;
            } else {
                $i = $index;
            }

            $this->line("<fg=yellow> $i </> $table");
        });

        $input   = $this->ask('Select tables to dump, separated by space');
        $options = collect(explode(' ', $input));

        $options->each(function($option) use ($tables, $disk) {

            if(!isset($tables[$option])){
                $this->error('Invalid option ' . $option);
            }

            $this->exportTable($tables[$option], $disk);
        });

        return CommandAlias::SUCCESS;
    }

    /**
     * @param string $table
     * @param FilesystemAdapter $disk
     * @throws CannotSetParameter
     */
    private function exportTable(string $table, FilesystemAdapter $disk) : void
    {
        $path = $disk->path('db/tables');

        if(!is_dir($path)){
            mkdir($path, 0777, true);
        }

        $file = $path . '/' . $table .   '.sql';

        MySql::create()
            ->setHost(      config('database.connections.mysql.host'))
            ->setDbName(    config('database.connections.mysql.database'))
            ->setUserName(  config('database.connections.mysql.username'))
            ->setPassword(  config('database.connections.mysql.password'))
            ->includeTables($table)
            ->dumpToFile($file);

        $this->lineInfo('Dumped table ' . $table . ' to ' . $file);
    }
}


