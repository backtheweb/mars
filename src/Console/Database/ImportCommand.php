<?php

namespace Backtheweb\Mars\Console\Database;

use Backtheweb\Mars\Traits\Commands\Helpers;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImportCommand extends Command
{

    use Helpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mars:db:import {file?} {{--disk=local}}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump database from file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->option('disk'));
        $file = $this->argument('file');

        if(!$file) {

            $fileData = collect();
            $files    = collect($disk->files('db/dumps'));

            $files->reject(function($file) {
                // reject files that are not sql
                return !preg_match('/\.sql$/', $file);
            })->each(function($file) use ($fileData, $disk) {
                $fileData->push([
                    'file' => $file,
                    'date' => $disk->lastModified($file )
                ]);
            });

            $file = $fileData->sortByDesc('date')->pluck('file')->first();

            if(!$file) {
                $this->error('No dumps found');
                return Command::FAILURE;
            }
        }

        if(!$disk->exists($file)) {
            $this->error($file . ' not exist');
            return Command::FAILURE;
        }

        $agree = $this->confirm('Import ' . $file . '?');

        if($agree != 'y') {

            $this->lineInfo('Aborted');
            return Command::FAILURE;
        }

        $this->clearScreen();
        $this->call('db:wipe');

        DB::unprepared(file_get_contents($disk->path($file)));

        $this->lineInfo('Imported from ' . $file . ' to ' . config('database.connections.mysql.database') . ' database');

        return Command::SUCCESS;
    }
}
