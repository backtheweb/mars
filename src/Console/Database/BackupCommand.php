<?php

namespace Backtheweb\Mars\Console\Database;

use Backtheweb\Mars\Actions\DumpDataBaseAction;
use Backtheweb\Mars\Traits\Commands\Helpers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class BackupCommand extends Command
{
    use Helpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mars:db:backup {--disk=local}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup database {--disk=local}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param DumpDataBaseAction $action
     * @return int
     * @throws \Exception
     */
    public function handle(DumpDataBaseAction $action):int
    {
        $disk = $this->option('disk');

        try {
            $file = $action->execute();
            $this->lineInfo('Dump to ' . $file . ' from ' . config('database.connections.mysql.database') . ' database');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return Command::FAILURE;
        }

        if($disk !== 'local'){
            Storage::disk($disk)->put(basename($file), file_get_contents($file));
            $this->lineInfo('Dump uploaded to ' . $disk . ' disk');
            unlink($file);
            $this->lineInfo('Dump deleted from local disk');
        }

        return Command::SUCCESS;
    }
}
