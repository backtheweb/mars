<?php

namespace Backtheweb\Mars\Console\Database;

use Backtheweb\Mars\Traits\Commands\Helpers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

class ImportTableCommand extends Command
{

    use Helpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mars:db:import-tables {--table} {{--disk=local}}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump database from file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        /** @var FilesystemAdapter $disk */
        $disk  = Storage::disk($this->option('disk'));
        $table = $this->option('table');

        if(!$table) {

            $fileData = collect();
            $files    = collect($disk->files('db/tables'));

            $files->reject(function($file) {
                // reject files that are not sql
                return !preg_match('/\.sql$/', $file);
            })->each(function($file) use ($fileData, $disk) {
                $fileData->push([
                    'file' => $file,
                    'date' => $disk->lastModified($file )
                ]);
            });

            if(!$files->count()){
                $this->error('No files found on storage app folder');
                return Command::FAILURE;
            }

            $files->each(function($name, $index) {

                if($index < 10){
                    $i = ' ' . $index;
                } else {
                    $i = $index;
                }

                $this->line("<fg=yellow> $i </> $name");
            });

            $input   = $this->ask('Select tables to dump, separated by space');
            $options = collect(explode(' ', $input));

            $options->each(function($option) use ($files, $disk) {

                if(!isset($files[$option])){
                    $this->error('Invalid option ' . $option);
                }

                $this->import($files[$option], $disk);
            });

            return Command::SUCCESS;
        }

        $this->import('db/tables/' . $table . '.sql', $disk);

        return Command::SUCCESS;
    }

    /**
     * @param string $file
     * @param FilesystemAdapter $disk
     * @return void
     */
    private function import(string $file, FilesystemAdapter $disk): void
    {

        if(!$disk->exists($file)){
            $this->error($file . 'not found on storage app folder');
            return;
        }

        DB::unprepared(file_get_contents($disk->path($file)));

        $this->lineDone('Import from ' . $file . ' to ' . config('database.connections.mysql.database') . ' database');
    }
}
