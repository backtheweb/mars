<?php

namespace Backtheweb\Mars\Console;

use Illuminate\Console\Command;

class MarsCommand extends Command
{
    protected $signature = 'mars';

    protected $description = 'mars inspire';

    public function handle() : int
    {
        $this->call('inspire');

        return Command::SUCCESS;
    }
}
