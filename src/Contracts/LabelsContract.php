<?php

namespace Backtheweb\Mars\Contracts;

interface LabelsContract
{
    /**
     * A html string label
     */
    public function label(): string;

    /**
     * A text plain label
     */
    public function textLabel(): string;
}
