<?php

namespace Backtheweb\Mars;

use Backtheweb\Mars\Console\Database\BackupCommand;
use Backtheweb\Mars\Console\Database\DumpCommand;
use Backtheweb\Mars\Console\Database\DumpTablesCommand;
use Backtheweb\Mars\Console\Database\ImportCommand;
use Backtheweb\Mars\Console\Database\ImportTableCommand;
use Backtheweb\Mars\Console\MarsCommand;
use Illuminate\Support\ServiceProvider;

class MarsServiceProvider extends ServiceProvider
{

    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/mars.php', 'mars');
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MarsCommand::class,
                BackupCommand::class,
                DumpCommand::class,
                DumpTablesCommand::class,
                ImportCommand::class,
                ImportTableCommand::class,
            ]);

            $this->publishes([
                __DIR__.'/../config/mars.php' => config_path('mars.php'),
            ], 'config');
        }
    }
}
