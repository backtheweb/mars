<?php

namespace Backtheweb\Mars\Traits\Commands;

trait Helpers
{
    function clearScreen() : void
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            system('cls');
        } else {
            system('clear');
        }
    }

    function lineInfo(string $message = ''): void
    {
        $this->line('  <bg=blue;fg=white> INFO </> ' . $message);
    }

    function lineDone(string $message = ''): void
    {
        $this->line('  <bg=green;fg=black> Done </> ' . $message);
    }
}
