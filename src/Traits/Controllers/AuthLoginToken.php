<?php

namespace Backtheweb\Mars\Traits\Controllers;

use Illuminate\Http\RedirectResponse;
use \Laravel\Sanctum\PersonalAccessToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait AuthLoginToken
{

    /**
     * @param Request $request
     * @param string $token
     * @return RedirectResponse
     */
    public function loginToken(Request $request, string $token): RedirectResponse
    {
        try {
            $token = PersonalAccessToken::firstWhere('token', decrypt($token));
        } catch (\Exception $e) {
            abort('403');
        }

        if (!$token->tokenable instanceof User) {
            abort('403');
        }

        Auth::guard('web')->login($token->tokenable);

        $url = $request->hasAny('redirect') ?
            base64_decode($request->get('redirect')) :
            route('dashboard');

        return redirect($url);
    }
}
