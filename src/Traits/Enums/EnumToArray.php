<?php

namespace Backtheweb\Mars\Traits\Enums;

trait EnumToArray
{
    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function array(): array
    {
        return array_combine(self::values(), self::names());
    }

    public static function arrayWithKeys(): array
    {
        return array_combine(self::names(), self::values());
    }

    public static function arrayWithKeysAndValues(): array
    {
        return array_combine(self::names(), self::cases());
    }

    public static function except(...$values): array
    {
        return array_diff(self::values(), $values);
    }


}
