<?php

namespace Backtheweb\Mars\Traits\Models;

use Illuminate\Support\Facades\Route;

trait ResourcePermalink
{
    public function getResourcePermalink(string $action = 'show'): string
    {
        $route = $this->getTable() . '.' . $action;

        if(!Route::has($route)){
            return '#';
        }

        return route($route, $this->getKey());
    }
}
