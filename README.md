# mars

## Installation

Install the package via composer:

```bash
$ composer require backtheweb/mars:dev-main
```

Publish the config file:

```bash
$ php artisan vendor:publish --provider="Backtheweb\Mars\MarsServiceProvider" --tag="config"
```

To start developing, run composer install on the package

```bash
$ cd {{ path }}
$ composer install
```

## Run tests

```bash
$ ./vendor/bin/phpunit
```
